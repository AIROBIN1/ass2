#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/sched.h>

#define ANCESTOR_NAME_LEN 16
struct process_info {
    long pid; /* Process ID */
    char name[ANCESTOR_NAME_LEN]; /* Program name of process */
    long state; /* Current process state */
    long uid; /* User ID of process owner */
    long nvcsw; /* # voluntary context switches */
    long nivcsw; /* # involuntary context switches */
    long num_children; /* # children process has */
    long num_siblings; /* # sibling process has */
};

SYSCALL_DEFINE3(
        process_ancestors, /* syscall name for macro */
        struct process_info*, info_array, /* array of process info strct */
        long, size, /* size of the array */
        long*, num_filled) /* # elements written to array */
{
    struct process_info process_info_k;
    long num_filled_k = 0;
    long num_children = 0;
    long num_siblings =0;
    struct task_struct* cur = current;
    struct list_head *child_head;
    struct list_head *sibling_head;
    struct list_head *child_node;
    struct list_head *sibling_node;
    if( size <= 0 ) {
        return -EINVAL; 
    } else if ( num_filled == NULL || info_array == NULL) {
        return -EFAULT;
    }
    while(cur && num_filled_k<size) {
        process_info_k.pid = cur->pid;
        memcpy(process_info_k.name, cur->comm, sizeof(cur->comm));
        process_info_k.state = cur->state;
        process_info_k.uid = (cur->cred)->uid.val;
        process_info_k.nvcsw = cur->nvcsw;
        process_info_k.nivcsw = cur->nivcsw;
        num_children = 0;
        num_siblings =0;
        child_head = &(cur->children);
        sibling_head = &(cur->sibling);
        child_node = child_head;
        sibling_node = sibling_head;
        while(child_node){
            ++num_children;
            if( child_node->next == child_head ) {
                break;
            } else {
                child_node = child_node->next;
            }
        }
        while(sibling_node){
            ++num_siblings;
            if( sibling_node->next == sibling_head ) {
                break;
            } else {
                sibling_node = sibling_node->next;
            }
        }
        num_children--; //exclude its parent
        num_siblings--; //exclude itself
        process_info_k.num_children = num_children;
        process_info_k.num_siblings = num_siblings;
        if(copy_to_user(info_array, &process_info_k, sizeof(process_info_k))){
            return -EFAULT;
        }
        ++info_array;
        ++num_filled_k; 
        if(cur->parent == cur) {
            break;
        }
        cur = cur->parent;
    }
    if(copy_to_user(num_filled, &num_filled_k, sizeof(long))){
        return -EFAULT;
    }else{
        return 0;
    }
}
