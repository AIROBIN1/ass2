#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <assert.h>

#define _TEST2_ 549 // for a 64 bit system

struct array_stats_s {
    long min;
    long max;
    long sum;
};

int main(int argc, char *argv[])
{
    struct array_stats_s stats;
    long test1[5] = { -5, 11, 9, 2, 3 };
    long test2[6] = { 1, 0, -4, 2, 99, 8 };
    int ret = 0;
    ret = syscall(_TEST2_, &stats, test1, 5);
    assert(ret == 0);
    printf( "test1: min %ld, max %ld, sum %ld \n", 
            stats.min, stats.max, stats.sum ); 
    ret = syscall(_TEST2_, &stats, test2, 6);
    assert(ret == 0);
    printf( "test2: min %ld, max %ld, sum %ld \n", 
            stats.min, stats.max, stats.sum ); 
    long s = -1;
    ret = syscall(_TEST2_, &stats, test1, s);
    assert(ret != 0);
    ret = syscall(_TEST2_, NULL, NULL, 1);
    assert(ret != 0);
    return 0;
}
