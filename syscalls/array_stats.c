#include <linux/kernel.h>
#include <linux/syscalls.h>

struct array_stats_s {
    long min;
    long max;
    long sum;
};

SYSCALL_DEFINE3(
        array_stats, /* syscall name */
        struct array_stats_s*, stats, /* where to write stats */
        long*, data, /* data to process */
        long, size) /* # values in data */
{
    long min = 0;
    long max = 0;
    long sum = 0;
    struct array_stats_s stats_k;
    long data_k = 0;
    long i = 0;
    if(size <= 0) {
        return -EINVAL; 
    } else if(stats == NULL || data == NULL) {
        return -EFAULT; 
    }
    if(copy_from_user(&data_k, data, sizeof(long))){
        return -EFAULT;
    }
    min = data_k, max = data_k;
    for(; i < size; ++i){
        if( copy_from_user(&data_k, &(data[i]), sizeof(long)) ) {
            return -EFAULT;
        }
        min = (data_k < min) ? data_k : min;
        max = (data_k > max) ? data_k : max;
        sum += data_k;
    }
    stats_k.min = min;
    stats_k.max = max;
    stats_k.sum = sum;
    if(copy_to_user(stats, &stats_k, sizeof(stats_k))){
        return -EFAULT;
    } else {
        return 0;
    }
}
