#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <assert.h>

#define _TEST3_ 550 // for a 64 bit system
#define ANCESTOR_NAME_LEN 16
struct process_info {
    long pid; /* Process ID */
    char name[ANCESTOR_NAME_LEN]; /* Program name of process */
    long state; /* Current process state */
    long uid; /* User ID of process owner */
    long nvcsw; /* # voluntary context switches */
    long nivcsw; /* # involuntary context switches */
    long num_children; /* # children process has */
    long num_siblings; /* # sibling process has */
};

int main(int argc, char *argv[])
{
    struct process_info test1[4];
    long size1 = 4;
    struct process_info test2[8];
    long size2 = 8;
    long num_filled_1;
    long num_filled_2;
    int ret = 0;
    ret = syscall(_TEST3_, test1, size1, &num_filled_1);
    assert(ret == 0);
    printf("test1:, filled elements: %ld \n", num_filled_1);
    for(long i = 0; i < num_filled_1; ++i) {
        printf("pid %ld, name %s, state %ld, uid %ld, "
               "nvcsw %ld, nivcsw %ld, children %ld, siblings %ld \n",
               test1[i].pid, test1[i].name, test1[i].state, test1[i].uid, test1[i].nvcsw, 
               test1[i].nivcsw, test1[i].num_children, test1[i].num_siblings);
    }
    ret = syscall(_TEST3_, test2, size2, &num_filled_2);
    assert(ret == 0);
    printf("test2:, filled elements: %ld \n", num_filled_2);
    for(long i = 0; i < num_filled_2; ++i) {
        printf("pid %ld, name %s, state %ld, uid %ld, "
               "nvcsw %ld, nivcsw %ld, children %ld, siblings %ld \n",
               test2[i].pid, test2[i].name, test2[i].state, test2[i].uid, test2[i].nvcsw, 
               test2[i].nivcsw, test2[i].num_children, test2[i].num_siblings);
    }
    ret = syscall(_TEST3_, NULL, size1, &num_filled_1);
    assert(ret != 0);
    long invalid_size = -5;
    ret = syscall(_TEST3_, test2, invalid_size, &num_filled_2);
    assert(ret != 0);
    return 0;
}
